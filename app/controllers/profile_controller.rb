class ProfileController < ApplicationController
  def user_params
    params.require(:profile).permit(:name, :gender, :username,:bio, :birth_date)
  end
  def index
    @profile = Profile.all
  end
  def new
    @profile = Profile.all
    @profile = Profile.new
  end
  def create
    @profile = Profile.create(user_params)
    if @profile.save
      redirect_to '/login'
    else 
      render 'new'
    end
  end
  def update
    @profile = Profile.find(params[:id])
    @profile.update(name: params[:name], gender: params[:gender], birth_date: params[:birth_date], bio: params[:bio], username: params[:username])
    redirect_to profile_path(@profile)
  end
  def show
    @profile = Profile.find(params[:id])
  end
end
