class SessionsController < ApplicationController
  def new
  end
  def create

    user = Signup.find_by(username: params[:session][:username].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in user
      remember user
      redirect_to '/health'
    else
      flash.now[:danger] = 'Invalid username/password combination'
      render 'new'
    end
  end
  def show
    @user = Signup.find(params[:user_id])
  end
  def destroy
    log_out
    redirect_to about_path
  end
end
