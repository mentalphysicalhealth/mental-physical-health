class SignupsController < ApplicationController
  def user_params
    params.require(:signup).permit(:username, :password, :password_confirmation, :email)
  end
  def index
    @user = Signup.all
  end
  def new
    @user = Signup.all
    @user = Signup.new
  end
  def create
    @user = Signup.new(user_params)
    if @user.save
      log_in @user
      redirect_to '/profile'
    else
      render 'new'
    end
  end
  def show
    @user = Signup.find(params[:id])
  end
end
