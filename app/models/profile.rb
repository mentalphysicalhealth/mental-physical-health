class Profile < ApplicationRecord
  validates :name, presence: true
  validates :gender, presence: true
  validates :username, presence: true
  validates :bio, presence: true
  validates :birth_date, presence: true
end

