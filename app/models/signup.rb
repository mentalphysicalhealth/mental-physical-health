class Signup < ApplicationRecord
  attr_accessor :remember_token
  before_save { email.downcase! }
  validates :username, presence: true
  validates :username, length: { maximum: 50 }
  validates :password, presence: true
  validates :password, length: { maximum: 20 }
  validates :password_confirmation, presence: true 
  validates :password_confirmation, length:{ maximum: 20 }
  validates :email, presence: true
  validates :email, length: { maximum: 255 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, format: { with: VALID_EMAIL_REGEX }
  validates :email, uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, presence: true
  validates :password, length: { minimum: 6 }
  def Signup.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? Bcrypt::Engine::MIN_COST : BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  def Signup.new_token
    SecureRandom.urlsafe_base64
  end
  def remember
    self.remember_token = Signup.new_token
    update_attribute(:remember_digest, Signup.digest(remember_token))
  end
  def authenticated?(remember_token)
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end
  def forget
    update_attribute(:remember_digest, nil)
  end
end
