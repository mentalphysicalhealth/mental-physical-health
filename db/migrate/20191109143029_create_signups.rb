class CreateSignups < ActiveRecord::Migration[6.0]
  def change
    create_table :signups do |t|
      t.string :username
      t.string :password
      t.string :re_password
      t.string :email

      t.timestamps
    end
  end
end
