class AddIndexToUsernamePasswordEmail < ActiveRecord::Migration[6.0]
  def change
    add_index :signups,:email, unique: true
  end
end
