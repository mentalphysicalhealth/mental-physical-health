class AddPasswordDigestToSignups < ActiveRecord::Migration[6.0]
  def change
    add_column :signups, :password_digest, :string
  end
end
