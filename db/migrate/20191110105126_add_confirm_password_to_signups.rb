class AddConfirmPasswordToSignups < ActiveRecord::Migration[6.0]
  def change
    add_column :signups, :password_confirmation, :string
  end
end
