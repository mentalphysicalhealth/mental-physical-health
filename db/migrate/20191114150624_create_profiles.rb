class CreateProfiles < ActiveRecord::Migration[6.0]
  def change
    create_table :profiles do |t|
      t.string :name
      t.string :gender
      t.string :username
      t.string :bio
      t.string :birth_date

      t.timestamps
    end
  end
end
