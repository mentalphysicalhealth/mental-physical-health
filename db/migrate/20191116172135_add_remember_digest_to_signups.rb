class AddRememberDigestToSignups < ActiveRecord::Migration[6.0]
  def change
    add_column :signups, :remember_digest, :string
  end
end
