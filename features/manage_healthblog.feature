Feature: Manage health
  In order to [goal]
  [stakeholder]
  wants [behaviour]

  Scenario: I click Health at sidebar
    Given I am on the health page
    When I click on "readmore1" button at the first blog of Health Blog page
    Then this should go to the health blog1 page
  Scenario: I click Health at sidebar
    Given I am on the health page
    Then I click on "readmore2" button at the second blog of Health Blog page
    Then this should go to the health blog2 page
  Scenario: I click Health at sidebar
    Given I am on the health page
    Then I click on "readmore3" button at the third blog of Health Blog page
    Then this should go to the health blog3 page
  Scenario: I click Health at sidebar
    Given I am on the health page
    Then I click on "readmore4" button at the fourth blog of Health Blog page
    Then this should go to the health blog4 page    
  Scenario: I click Health at sidebar
    Given I am on the health page
    Then I click on "raadmore5" button at the fifth blog of Health Blog page
    Then this should go to the health blog5 page    
  Scenario: I click Health at sidebar
    Given I am on the health page
    Then I click on "readmore6" button at the sixth blog of Health Blog page
    Then this should go to the health blog6 page
  Scenario: I want to change blog page to Mental blog
    Given I am on the health page
    Then I click link "Mental" at the sidebar of Health Blog page
    Then this should go to the mental page
  Scenario: I want to change blog page to Food For You blog
    Given I am on the health page
    Then I click link "Food For You" at the sidebar of Health Blog page
    Then this should go to the food page
  Scenario: I want to change blog page to About us page
    Given I am on the health page
    Then I click link "About Us" at the sidebar of Health Blog page
    Then this should go to the about page
  Scenario: I want to logout 
    Given I am on the health page
    When I look at the right top side of the health page
    Then I click "Logout" which is in the icon dropdown option
    Then this should go to the home page 
