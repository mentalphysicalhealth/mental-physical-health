Feature: Manage profile
  In order to [goal]
  [stakeholder]
  wants [behaviour]

  Scenario: I click Mental at sidebar
    Given I am on the mental page
    When I click on "readmore1" button at the first blog of Mental Blog page
    Then this should go to the mental blog1 page
  Scenario: I click Mental at sidebar
    Given I am on the mental page
    Then I click on "readmore2" button at the second blog of Mental Blog page
    Then this should go to the mental blog2 page
  Scenario: I click Mental at sidebar
    Given I am on the mental page
    Then I click on "readmore3" button at the third blog of Mental Blog page
    Then this should go to the mental blog3 page
  Scenario: I click Mental at sidebar
    Given I am on the mental page
    Then I click on "readmore4" button at the fourth blog of Mental Blog page
    Then this should go to the mental blog4 page    
  Scenario: I click Mental at sidebar
    Given I am on the mental page
    Then I click on "raadmore5" button at the fifth blog of Mental Blog page
    Then this should go to the mental blog5 page    
  Scenario: I click Mental at sidebar
    Given I am on the mental page
    Then I click on "readmore6" button at the sixth blog of Mental Blog page
    Then this should go to the mental blog6 page
  Scenario: I want to change blog page to Health blog
    Given I am on the mental page
    Then I click link "Health" at the sidebar of Mental Blog page
    Then this should go to the health page
  Scenario: I want to change blog page to Food For You blog
    Given I am on the mental page
    Then I click link "Food For You" at the sidebar of Mental Blog page
    Then this should go to the food page
  Scenario: I want to change blog page to About us page
    Given I am on the mental page
    Then I click link "About Us" at the sidebar of Mental Blog page
    Then this should go to the about page
  Scenario: I want to logout 
    Given I am on the mental page
    When I look at the right top of the mental page
    Then I click link "Logout" which is in the icon dropdown option
    Then this should go to the home page