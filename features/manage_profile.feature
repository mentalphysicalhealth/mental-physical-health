Feature: Manage profile
  In order to [goal]
  [stakeholder]
  wants [behaviour]
  
  Scenario: fill in information to user's profile and choose male gender
    Given I am on the profile page
    And I fill in "profile[name]" with "mantita weangtham"
    And I fill in "profile[username]" with "username 1"
    And I fill in "profile[birth_date]" with "01/08/2542"
    And I fill in "profile[bio]" with "Hi, I am Chun nice to meet you"
    And I choose "profile_gender_male"
    And I click on "Save Change" button
    Then it should see the about page
  Scenario: fill in information to user's profile and choose female gender
    Given I am on the profile page
    And I fill in "profile[name]" with "nantapat tianpanudech"
    And I fill in "profile[username]" with "username 2"
    And I fill in "profile[birth_date]" with "04/22/2540"
    And I fill in "profile[bio]" with "Hi, I am may nice to meet you"
    And I choose "profile_gender_female"
    And I click on "Save Change" button
    Then it should see the about page
  Scenario: fill in information to user's profile and choose lgbt gender
    Given I am on the profile page
    And I fill in "profile[name]" with "John Doe"
    And I fill in "profile[username]" with "username 3"
    And I fill in "profile[birth_date]" with "06/27/2536"
    And I fill in "profile[bio]" with "Hi, I am john nice to meet you"
    And I choose "profile_gender_lgbt"
    And I click on "Save Change" button
    Then it should see the about page
