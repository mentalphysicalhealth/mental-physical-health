Feature: Manage sessions
  In order to [goal]
  [stakeholder]
  wants [behaviour]

  Scenario: Login with valid credentials
    Given I am on the login page
    When I fill in "session[username]" with "nantapat"
    And I fill in "session[password]" with "password"
    Then I click "Login" button
    Then this should go to the home page
  Scenario: Come to Login page and click signup button
    Given I am on the login page
    Then I click "Signup" button
    Then this should go to the new signup page
