Feature: Manage signups
  In order to [goal]
  [stakeholder]
  wants [behaviour]
  
  Scenario: Register new signup
    Given I am on the new signup page
    When I fill in "signup[username]" with "username 1"
    And I fill in "signup[password]" with "password"
    And I fill in "signup[password_confirmation]" with "password"
    And I fill in "signup[email]" with "abc@mail.com"
    And I click "Submit"
