Given /^the following mentalblog:$/ do |mentalblog|
  Mentalblog.create!(sessions.hashes)
end
When /^I click on "([^"]*)" button at the first blog of Mental Blog page$/ do |button|
  click_button "readmore1"
end   
Then /^I click on "([^"]*)" button at the second blog of Mental Blog page$/ do |button|
  click_button "readmore2"
end   
Then /^I click on "([^"]*)" button at the third blog of Mental Blog page$/ do |button|
  click_button "readmore3"
end   
Then /^I click on "([^"]*)" button at the fourth blog of Mental Blog page$/ do |button|
  click_button "readmore4"
end   
Then /^I click on "([^"]*)" button at the fifth blog of Mental Blog page$/ do |button|
  click_button "readmore5"
end
Then /^I click on "([^"]*)" button at the sixth blog of Mental Blog page$/ do |button|
  click_button "readmore6"
end   
Then /^(?:|I )click link "([^"]*)" at the sidebar of Mental Blog page$/ do |link|
  click_link(link)
end
When /^I look at the right top of (.+)$/ do |page_name|
  visit path_to(page_name)
end
Then /^(?:|I )click link "([^"]*)" which is in the icon dropdown option$/ do |link|
  click_link "Logout"
end
Then /^(?:|it )should go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
