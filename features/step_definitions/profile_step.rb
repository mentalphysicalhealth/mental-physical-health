Given /^the following profile:$/ do |profile|
  Profile.create!(Profile.hashes)
end
When /^I fill in "([^"]*)" with "([^"]*)$/ do |value, field_id|
  fill_in field_id, :with => value
end
And /^I fill in "([^"]*)" with "([^"]*)$/ do |value, field_id|
  fill_in field_id, :with => value
end
And /^(?:|I )choose "([^"]*)$/ do |field|
  choose field
end
And /^I click on "([^"]*)" button$/ do |button|
  click_button "Save Change"
end
Then /^(?:|it )should see (.+)$/ do |page_name|
  visit path_to(page_name)
end
