Given /^the following sessions:$/ do |signups|
  Sessions.create!(sessions.hashes)
end
When /^I fill in "([^"]*)" with "([^"]*)$/ do |value, field_id|
  fill_in field_id, :with => value
end
And /^I fill in "([^"]*)" with "([^"]*)$/ do |value, field_id|
  fill_in field_id, :with => value
end
Then /^I click "([^"]*)" button$/ do |button|
  click_button "Login"
end
Then /^(?:|this )should go to (.+)$/ do |page_name|
  visit path_to(page_name)
end