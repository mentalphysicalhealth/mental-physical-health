Given /^the following signups:$/ do |signups|
  Signup.create!(signups.hashes)
end
When /^I fill in "([^"]*)" with "([^"]*)$/ do |value, field_id|
  fill_in field_id, :with => value
end
And /^I fill in "([^"]*)" with "([^"]*)$/ do |value, field_id|
  fill_in field_id, :with => value
end
And /^I click "([^"]*)"$/ do |button|
  click_button "Submit"
end



