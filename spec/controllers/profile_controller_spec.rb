require 'rails_helper'

RSpec.describe ProfileController, type: :controller do
  describe "GET #new" do
    it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST #create" do
    context  "with valid params" do
      it "save information of user" do
        expect {
          post :create, params: {profile: {name: "mantita weangtham", gender: "female",username: "chunik",bio: "hi I am chun I love dog",birth_date: "08/01/1999"}}
      }.to change(Profile, :count).by(1)
      end
    end
  end
  let!(:profile) { create(:profile)}
  describe "PATCH #update" do
    it 'should be update personal info' do
      params = {
        name: 'Jane Doe',
        gender: 'Female',
        username: 'Jane',
        bio: 'I am Jane',
        birth_date: '13/09/1987'
      }
      put :update, params: { id: profile.id, profile: params}
      profile.reload
      params.keys.each do |key|
        expect(profile.attributes[key.to_s]).to eq params[key]
      end
    end
  end
end
