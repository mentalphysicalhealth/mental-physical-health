require 'rails_helper'
RSpec.describe SignupsController, type: :controller do
  
  describe "GET #new" do
    it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
    end
  end
  describe "POST #create" do
    context "with valid params" do
      it "create a new account" do
        expect {
          post :create, params: {signup: {username: "bob", password: "password", password_confirmation: "password", email: "abcd@mail.com"}}
      }.to change(Signup, :count).by(1)
      end
    end
  end
end
