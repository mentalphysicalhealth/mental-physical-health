FactoryBot.define do
  factory :health do
    
  end

  factory :food do
    
  end

  factory :profile do
    name {"Jane Doe"}
    gender {"Female"}
    username {"Jane"}
    bio {"I am Jane"}
    birth_date {"13/09/1987"}
  end
end