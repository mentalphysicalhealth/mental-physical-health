require 'rails_helper'
RSpec.describe Profile, type: :model do
  # let (:user) {Profile.new(:name => "mantita weangtham",:gender => "female",:username => "chunik",:bio => "hi I am chun I love dog",:birth_date => "08/01/1999")}
  # it "should be valid" do
  #   expect(user.valid?).to be true
  # end
  # it "should be present name" do
  #   user.name = ""
  #   expect(user.valid?).to be false
  # end
  before(:all) do
    @user1 = create(:profile)
  end
  it "is valid with valid attributes" do
    expect(@user1).to be_valid
  end
  it "has a unique name" do
    user2 = build(:profile, name: nil)
    expect(user2).to_not be_valid
  end
  
  it "has a  username" do
    user2 = build(:profile, username: nil)
    expect(user2).to_not be_valid
  end
  
  it "is valid with gender" do 
    user2 = build(:profile, gender: nil)
    expect(user2).to_not be_valid
  end
  
  it "is valid with bio" do 
    user2 = build(:profile, bio: nil)
    expect(user2).to_not be_valid
  end
  
  it "is valid with birth date" do
    user2 = build(:profile, birth_date: nil)
    expect(user2).to_not be_valid
  end
end
