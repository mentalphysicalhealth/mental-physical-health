require 'rails_helper'
RSpec.describe Signup, type: :model do
  let (:user) { Signup.new(:username => "nantapat", :password => "password1", :password_confirmation => "password1", :email => "abc@gmail.com") }
  it "should be valid" do
    expect(user.valid?).to be true
  end
  it "should be present the username" do
    user.username = ""
    expect(user.valid?).to be false
  end
  it "should be present the password" do
    user.password = " "
    expect(user.valid?).to be false
  end
  it "should be present the confirm password" do
    user.password_confirmation = ""
    expect(user.valid?).to be false
  end
  it "should be present the email" do
    user.email = ""
    expect(user.valid?).to be false
  end
  it "should not be too long username" do
    user.username = "a" * 51
    expect(user.valid?).to be false
  end
  it "should not be too long password" do
    user.password = "a" * 21
    expect(user.valid?).to be false
  end
  it "should not be too long password_confirmation" do
    user.password_confirmation = "a" * 21
    expect(user.valid?).to be false
  end
  it "should not be too long email" do
    user.email = "a" * 244 + "@example.com"
    expect(user.valid?).to be false
  end
  it "should accept valid address of email validation" do
    valid_address = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org first.last@foo.jp alice+bob@baz.cn]
    valid_address.each do |valid_address|
      user.email = valid_address
      expect(user.valid?).to be true
    end
  end
  it "should be unique email address" do
    duplicate_user = user.dup 
    duplicate_user.email = user.email.upcase
    user.save
    expect(duplicate_user.valid?).to be false
  end
  it "should be saved as lower-case email addresses" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    user.email = mixed_case_email
    user.save
    expect(mixed_case_email.downcase).to eq(user.reload.email)
  end
  it "should be present password (nonblank)" do
    user.password = user.password_confirmation = " " * 6
    expect(user.valid?).to be false
  end
  it "should have a minimum password length" do
    user.password = user.password_confirmation = "a" * 5
    expect(user.valid?).to be false
  end
end
