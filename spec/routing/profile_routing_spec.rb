require 'rails_helper'

RSpec.describe ProfileController, type: :routing do
  describe "routing" do
    it "routes to #new" do
      expect(:get => "/profile").to route_to("profile#new")
    end
    it "routes to #create" do
      expect(:post => "/profile/create").to route_to("profile#create")
    end
    it "routes to #update" do
      expect(:put => "/profile/update").to route_to("profile#update")
    end
  end
end
