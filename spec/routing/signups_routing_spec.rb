require "rails_helper"

RSpec.describe SignupsController, type: :routing do
  describe "routing" do
    it "routes to #new" do
      expect(:get=> "/signup").to route_to("signups#new")
    end
    it "routes to #create" do
      expect(:post=> "/signup/create").to route_to("signups#create")
    end
  end
end